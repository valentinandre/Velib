import {Injectable} from "@angular/core";
import {map} from "../pages/map/map";

@Injectable()
export class mapService {
  private stations: any = map;

  constructor() {
  }

  getMap() {
    return this.stations;
  }

  setMap(stations: any) {
    this.stations = stations;
  }
}
