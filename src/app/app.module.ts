import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MyApp } from './app.component';
import {a_propos} from "../pages/a_propos/a_propos";
import {map} from "../pages/map/map";
import {dataService} from "./dataService";
import {mapService} from "./mapService";
import {alertes} from "../pages/alertes/alertes";
import {stations} from "../pages/stations/stations";
import {favoris} from "../pages/favoris/favoris";

@NgModule({
  declarations: [
    MyApp,
    map,
    a_propos,
    stations,
    favoris,
    alertes
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    map,
    a_propos,
    stations,
    favoris,
    alertes
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, mapService, dataService, Storage]
})
export class AppModule {}
