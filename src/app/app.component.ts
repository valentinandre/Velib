import { Component, ViewChild } from '@angular/core';
import {Nav, Platform, MenuController} from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import {a_propos} from "../pages/a_propos/a_propos";
import {map} from "../pages/map/map";
import {favoris} from "../pages/favoris/favoris"
import {stations} from "../pages/stations/stations";
import {alertes} from "../pages/alertes/alertes";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = map;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public menu : MenuController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title : 'Favoris', component: favoris},
      { title : 'Stations', component: stations},
      { title : 'Alertes', component: alertes},
      { title: 'Aide', component: a_propos }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.menu.swipeEnable(false, "menu");
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }
}
