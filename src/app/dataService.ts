/**
 * Created by valentinandre on 08/01/2017.
 */
/* Notice that the imports have slightly changed*/
import {Injectable} from "@angular/core";

@Injectable()
export class dataService {
  private dataUrl;
  private pisteCyclableUrl;

  constructor() {
    this.dataUrl =  'https://download.data.grandlyon.com/wfs/rdata?SERVICE=WFS&VERSION=2.0.0&outputformat=GEOJSON&request=GetFeature&typename=jcd_jcdecaux.jcdvelov&SRSNAME=urn:ogc:def:crs:EPSG::4171';
    this.pisteCyclableUrl = 'https://download.data.grandlyon.com/wfs/grandlyon?SERVICE=WFS&VERSION=2.0.0&outputformat=GEOJSON&request=GetFeature&typename=pvo_patrimoine_voirie.pvoamenagementcyclable&SRSNAME=urn:ogc:def:crs:EPSG::4171';
  }

  getDataUrl() {
    return this.dataUrl;
  }

  getPisteCyclableUrl() {
    return this.pisteCyclableUrl;
  }
}
