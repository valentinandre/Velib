import {Component, trigger, state, style, transition, animate} from '@angular/core';

import {NavController} from 'ionic-angular';
import {Storage} from '@ionic/storage';

import {mapService} from '../../app/mapService';
import {map} from "../map/map";
import ol from 'openlayers';


@Component({
  selector: 'page-favoris',
  templateUrl: 'favoris.html',
  animations: [
    trigger('stationState', [
      state('inactive', style({
        height: '50px'
      })),
      state('active', style({
        height: '100px'
      })),
      transition('inactive => active', animate('400ms ease-in')),
      transition('active => inactive', animate('400ms ease-out'))
    ]),
    trigger('stationFavoris', [
      state('commun', style({
        visibility: 'hidden'
      })),
      state('favoris', style({
        visibility: 'visible'
      }))
    ])
  ]
})

export class favoris {

  map: map;
  items: any;
  storage: Storage;
  favoris: any;

  constructor(public navCtrl: NavController, private map_serv: mapService, storage: Storage) {
    this.map = this.map_serv.getMap();
    this.storage = storage;
  }

  ngOnInit(): void {
    this.initializeItems();
  }

  initializeItems() {
    let noms_station: string[] = [];
    var stations = this.map.olMap.getLayers().item(2).getSource().getFeatures();
    var utilisateur = this.map.olMap.getLayers().item(3).getSource().getFeatures()[0].getGeometry().getCoordinates();
    let compteur = 0;
    this.storage.keys().then((__zone_symbol__value) => {
        this.favoris = __zone_symbol__value;
      }
    ).then(() => {
        for (let station of stations) {
          if (this.favoris.indexOf(station.getProperties().number.toString()) != -1) {
            noms_station[compteur] = station.getProperties();
            var coordinate = station.getGeometry().getCoordinates();
            var line = new ol.geom.LineString([utilisateur, coordinate]);
            noms_station[compteur]["distance"] = Math.round(line.getLength());
            compteur++;
          }
        }

        noms_station.sort(function (a, b) {
          return a["distance"] - b["distance"];
        });
        this.items = noms_station;
        this.items.forEach(item => {
          item.state = 'inactive'
        });
      }
    );
  }

  itemTapped(ev, item) {
    if (item.state == 'inactive')
      item.state = 'active';
    else
      item.state = 'inactive';
  }

  clicFavoriDelete(item) {
    this.storage.remove(item.number.toString());
    item.favoris = 'commun';
    this.initializeItems();
  }
}
