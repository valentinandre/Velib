import {Component, OnInit, trigger, state, style, transition, animate, ViewChild} from '@angular/core';
import {mapService} from '../../app/mapService';
import {map} from "../map/map";
import {FormControl} from "@angular/forms";
import {NavController} from "ionic-angular";
import {Storage} from '@ionic/storage';
import ol from 'openlayers';


@Component({
  selector: 'page-stations',
  templateUrl: 'stations.html',
  animations: [
    trigger('stationState', [
      state('inactive', style({
        height: '50px'
      })),
      state('active', style({
        height: '100px'
      })),
      transition('inactive => active', animate('400ms ease-in')),
      transition('active => inactive', animate('400ms ease-out'))
    ]),
    trigger('stationFavoris', [
      state('commun', style({
        visibility: 'hidden'
      })),
      state('favoris', style({
        visibility: 'visible'
      }))
    ]),
    trigger('stationCommun', [
      state('commun', style({
        visibility: 'visible'
      })),
      state('favoris', style({
        visibility: 'hidden'
      }))
    ])
  ],
})

export class stations implements OnInit {

  @ViewChild('searchbarInput') bar;
  map: map;
  items: any;
  searchTerm: string = '';
  searchControl: FormControl;
  searching: any = false;
  storage: Storage;
  favoris: any;

  constructor(public navCtrl: NavController, private map_serv: mapService, storage: Storage) {
    this.map = this.map_serv.getMap();
    this.searchControl = new FormControl();
    this.storage = storage;
  }

  ngOnInit(): void {
    this.initializeItems();
  }

  ionViewDidLoad() {

    this.setFilteredItems();

    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {

      this.searching = false;
      this.setFilteredItems();

    });
  }

  onSearchInput() {
    this.searching = true;
  }

  onCancel(ev: any) {
    this.items = this.filterItems(this.searchTerm);
  }

  setFilteredItems() {
    this.items = this.filterItems(this.searchTerm);
  }

  itemTapped(ev, item) {
    if (item.state == 'inactive')
      item.state = 'active';
    else
      item.state = 'inactive';
  }

  initializeItems() {
    let noms_station: string[] = [];
    var stations = this.map.olMap.getLayers().item(2).getSource().getFeatures();
    var utilisateur = this.map.olMap.getLayers().item(3).getSource().getFeatures()[0].getGeometry().getCoordinates();
    let compteur = 0;
    for (let station of stations) {
      noms_station[compteur] = station.getProperties();
      var coordinate = station.getGeometry().getCoordinates();
      var line = new ol.geom.LineString([utilisateur, coordinate]);
      noms_station[compteur]["distance"] = Math.round(line.getLength());
      compteur++;
    }
    noms_station.sort(function (a, b) {
      return a["distance"] - b["distance"];
    });
    this.items = noms_station;
    this.items.forEach(item => {
      item.state = 'inactive'
    });

    this.storage.keys().then((__zone_symbol__value) => {
        this.favoris = __zone_symbol__value;
      }
    ).then(() => {
      compteur = 0;
      for (let station of stations) {
        if (this.favoris.indexOf(station.getProperties().number.toString()) == -1)
          noms_station[compteur]["favoris"] = "commun";
        else
          noms_station[compteur]["favoris"] = "favoris";

        compteur++;
      }
      }
    );
  }

  filterItems(searchTerm) {
    this.initializeItems();
    return this.items.filter((item) => {
      return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  onScroll(event) {
    this.bar.blur();
  }

  clicFavoriAdd(item) {
    this.storage.set(item.number.toString(), item.adress);
    item.favoris = 'favoris';
  }

  clicFavoriDelete(item) {
    this.storage.remove(item.number.toString());
    item.favoris = 'commun';
  }
}
