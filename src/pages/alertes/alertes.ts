import {Component, OnInit} from '@angular/core';
import { NavController } from 'ionic-angular';
import {map} from "../map/map";
import {mapService} from "../../app/mapService";

@Component({
  selector: 'page-alertes',
  templateUrl: 'alertes.html'
})

export class alertes implements OnInit {

  map: map;
  items: any;


  constructor(public navCtrl: NavController, private map_serv: mapService) {
    this.map = this.map_serv.getMap();

  }

  ngOnInit(): void {
    this.initializeItems();

  }

  ionViewDidLoad() {



  }



  itemTapped(ev,item)
  {
    if(item.state == 'inactive')
      item.state = 'active';
    else
      item.state = 'inactive';
  }

  initializeItems() {
    let noms_station: string[] = [];
    var stations = this.map.olMap.getLayers().item(2).getSource().getFeatures();
    let compteur = 0;

    for (let station of stations) {
      var prop = station.getProperties();

      if (prop.status == "CLOSED") {
        noms_station[compteur] = prop;
        compteur++;
      }
    }
    this.items = noms_station;
  }

}
