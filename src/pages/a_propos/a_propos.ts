import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-a_propos',
  templateUrl: 'a_propos.html'
})
export class a_propos {

  constructor(public navCtrl: NavController) {

  }

}
