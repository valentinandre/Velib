import ol from 'openlayers';
import {NavController} from 'ionic-angular';
import {Component, ViewChild, OnInit} from '@angular/core';
import {dataService} from "../../app/dataService";
import {Observable} from 'rxjs/Rx';
import {Geolocation} from 'ionic-native';
import {mapService} from "../../app/mapService";

@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
  providers: [dataService]
})

export class map implements OnInit {

  @ViewChild('map') olMap;
  data: dataService;
  latitude : any;
  longitude : any;
  coordonnees : any;
  isPiste : boolean;
  layer : any;
  filtre : any;

  constructor(public navCtrl: NavController, data: dataService, private map_serv: mapService) {
    this.data = data;
    this.latitude = 0;
    this.longitude = 0;
    this.coordonnees = ol.proj.transform([this.longitude, this.latitude], 'EPSG:4326', 'EPSG:3857');
    this.isPiste = true;
    this.layer = 1;
    this.filtre = 1;
  }

  ngOnInit() {

      //Geolocalisation de l'utilisateur
      Geolocation.getCurrentPosition().then((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        //Coordonnées initiales
        this.coordonnees = ol.proj.transform([this.longitude, this.latitude], 'EPSG:4326', 'EPSG:3857');
        view.setCenter(this.coordonnees);
        geolocalisation_feature.setGeometry(new ol.geom.Point(this.coordonnees));
      }, (err) => {
        this.coordonnees = ol.proj.transform([4.83213506219, 45.767734686399997], 'EPSG:4326', 'EPSG:3857');
        view.setCenter(this.coordonnees);
        geolocalisation_feature.setGeometry(new ol.geom.Point(this.coordonnees));
      });

      //Creation de la vue principale
      var view = new ol.View(
        {
          center: this.coordonnees,
          zoom: 15,
          maxZoom: 20
        }
      );

      //Gestion de la position de l'utilisateur
      var geolocalisation_style = new ol.style.Style(
        {
          zIndex: 2,
          image: new ol.style.Circle({
            radius: 12,
            fill: new ol.style.Fill({color: '#4c9cf6'}),
            stroke: new ol.style.Stroke({color: '#f6f6f6', width: 2})
          }),
        }
      );

      var geolocalisation_feature = new ol.Feature({
        id: 'geolocalisation',
        geometry: new ol.geom.Point(this.coordonnees),
        name: 'geolocalisation',
        size: 10
      });
      var geolocalisation_vectorSource = new ol.source.Vector({
        features: [geolocalisation_feature]
      });
      var geolocalisation_layer = new ol.layer.Vector(
        {
          source : geolocalisation_vectorSource,
          style: geolocalisation_style
        }
      );

      //Affichage des pistes cyclables
      var stylePiste = function (feature, resolution) {
        var geometries = feature.getGeometry();
        var line = geometries[1];

        var lineStyle = new ol.style.Style({
          zIndex:1,
          geometry: line,
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.1)'
          }),
          stroke: new ol.style.Stroke({
            color: '#3498db',
            width: 2
          })
        });

        return [lineStyle];

      };

      var stationLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
          url: this.data.getPisteCyclableUrl(),
          format: new ol.format.GeoJSON()
        }),
        style: stylePiste
      });

      //gestion des stations
      var station_style = function(feature) {
        var props = feature.getProperties();
        var num = props.number;
        if (props.available_bike_stands > 5 && props.status == "OPEN")
        {
          var style = new ol.style.Style(
            {
              image: new ol.style.Icon({
                scale: 0.17,
                src: 'assets/img/station_vert.png'
              }),
              text: new ol.style.Text({
                text: props.available_bikes.toString(),
                scale: 1.3,
                fill: new ol.style.Fill({
                  color: '#ffffff'
                })
              }),
              zIndex: num
            }
          );
        }
        else if (props.available_bike_stands < 5 && props.available_bike_stands > 0 && props.status == "OPEN"){
          var style = new ol.style.Style(
            {
              image: new ol.style.Icon({
                scale: 0.17,
                src: 'assets/img/station_orange.png'
              }),
              text: new ol.style.Text({
                text: props.available_bikes.toString(),
                scale: 1.3,
                fill: new ol.style.Fill({
                  color: '#ffffff'
                })
              }),
              zIndex: num
            }
          );
        }
        else if (props.status == "CLOSED"){
          var style = new ol.style.Style(
            {
              image: new ol.style.Icon({
                scale: 0.17,
                src: 'assets/img/station_noire.png'
              }),
              zIndex: num
            }
          );
        }
        else
        {
          var style = new ol.style.Style(
            {
              image: new ol.style.Icon({
                scale: 0.17,
                src: 'assets/img/station_rouge.png'
              }),
              text: new ol.style.Text({
                text: props.available_bikes.toString(),
                scale: 1.3,
                fill: new ol.style.Fill({
                  color: '#ffffff'
                })
              }),
              zIndex: num
            }
          );
        }
        return [style];
      }
      var station_layer = new ol.layer.Vector({
        source: new ol.source.Vector({
          url: this.data.getDataUrl(),
          format: new ol.format.GeoJSON(),
        }),
        style: station_style
      });

      //gestion de la pop up d'infos
      var container = document.getElementById('popup');
      var content = document.getElementById('popup-content');
      var title = document.getElementById('popup-title');
      var distance = document.getElementById('popup-distance');
      var closer = document.getElementById('popup-closer');
      var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
        element: container
      }));

      //sur clic de fermeture de la pop up
      closer.onclick = function() {
        overlay.setPosition(undefined);
        closer.blur();
        return false;
      };

      //création de la map
      this.olMap = new ol.Map({
        target : "map",
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          }), stationLayer, station_layer, geolocalisation_layer
        ],
        controls: ol.control.defaults({
          attribution : false,
          attributionOptions: ({
            collapsible: false
          })
        }),
        overlays: [overlay],
        view: view
      });

      //Rafraichissement automatique de la position de l'utilisateur
      Observable.interval(500).subscribe(x => {
        Geolocation.getCurrentPosition().then((position) => {
          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;
          //Coordonnées initiales
          this.coordonnees = ol.proj.transform([this.longitude, this.latitude], 'EPSG:4326', 'EPSG:3857');
          geolocalisation_feature.setGeometry(new ol.geom.Point(this.coordonnees));
        }, (err) => {
          this.coordonnees = ol.proj.transform([4.83213506219, 45.767734686399997], 'EPSG:4326', 'EPSG:3857');
          geolocalisation_feature.setGeometry(new ol.geom.Point(this.coordonnees));
        });
      });

      //Affichage des informations sur les stations
      this.olMap.on('singleclick', function(evt, layer) {
        var feature = this.forEachFeatureAtPixel(evt.pixel, function(feature, layer) {
          return feature;
        });
        if (feature) {
          var props = feature.getProperties();
          closer.innerHTML = '<img class="distance_popup" src="assets/img/cross.png">';
          //Si c'est pas un clic sur la position de l'utilisateur
          if (props.name != "geolocalisation")
          {
            //Si la station est fermée
            if (props.status == "CLOSED")
            {
              var coordinate = evt.coordinate;
              title.innerHTML = props.name;
              content.innerHTML = '<p class="content">Station fermée actuellement</p><img class="icon_popup" src="assets/img/closed.png">';

              //On calcule la distance
              var utilisateur = this.getLayers().item(3).getSource().getFeatures()[0].getGeometry().getCoordinates();
              var line = new ol.geom.LineString([utilisateur, coordinate]);

              if (line.getLength() >= 1000) {
                var length = (Math.round(line.getLength() / 1000 * 100) / 100) +
                  ' ' + 'km';
              } else {
                var length = Math.round(line.getLength()) +
                  ' ' + 'm';
              }
              distance.innerHTML = '<img class="distance_popup" src="assets/img/distance.png">' + '  ' + length.toString();

              overlay.setPosition(coordinate);
            }
            //Si la station n'est pas fermée
            else {
              var coordinate = evt.coordinate;
              title.innerHTML = props.name;
              content.innerHTML = '<table class="table_infos"><tr><th></th><th></th></tr>'+
                '<tr><td><img class="icon_popup" src="assets/img/bicycle.png"></td><td><img class="icon_popup" src="assets/img/parking.png"></td></tr>'+
                '<tr><td><p class="content">'+props.available_bikes+'</p></td><td><p class="content">'+props.available_bike_stands+'</p></td></tr>'+
                '</table>';

              //On calcule la distance
              var utilisateur = this.getLayers().item(3).getSource().getFeatures()[0].getGeometry().getCoordinates();
              var line = new ol.geom.LineString([utilisateur, coordinate]);
              if (line.getLength() >= 1000) {
                var length = (Math.round(line.getLength() / 1000 * 100) / 100) +
                  ' ' + 'km';
              } else {
                var length = Math.round(line.getLength()) +
                  ' ' + 'm';
              }
              distance.innerHTML = '<img class="distance_popup" src="assets/img/distance.png">' + '  ' + length.toString();

              overlay.setPosition(coordinate);
            }
          }
        }
      });

    this.map_serv.setMap(this);

    }

  //centrer la map sur l'utilisateur
  center() {
    // bounce by zooming out one level and back in
    var bounce = ol.animation.bounce({
      resolution: this.olMap.getView().getResolution() * 1.25
    });
    // start the pan at the current center of the map
    var pan = ol.animation.pan({
      source: this.olMap.getView().getCenter()
    });
    this.olMap.beforeRender(bounce);
    this.olMap.beforeRender(pan);
    // when we set the center to the new location, the animated move will
    // trigger the bounce and pan effects
    this.olMap.getView().setCenter(this.coordonnees);
  }

  //rafraichir les données des stations et leur visuel
  refresh() {
    this.olMap.getLayers().item(2).getSource().clear();
    document.getElementById("refresh_button").style.animation = "spin 0.8s";
    setTimeout(()=>{document.getElementById("refresh_button").style.removeProperty("animation");}, 1000);
  }

  displayPiste()
  {
    if (this.isPiste)
    {
      this.isPiste = false;
      this.olMap.getLayers().item(1).setVisible(false);
    }
    else
    {
      this.isPiste = true;
      this.olMap.getLayers().item(1).setVisible(true);
    }
  }

  changeLayer()
  {
    if (this.layer == 1)
    {
      this.layer ++;
      this.olMap.getLayers().item(0).setSource(new ol.source.TileArcGISRest({url:'http://server.arcgisonline.com/arcgis/rest/services/ESRI_StreetMap_World_2D/MapServer'}));
    }
    else
    {
      this.layer --;
      this.olMap.getLayers().item(0).setSource(new ol.source.OSM());
    }
  }

  filter()
  {
    var emptyStyle = new ol.style.Style(
      {
      }
    );
    var station_style = function() {
      var props = this.getProperties();
      if (props.available_bike_stands > 5 && props.status == "OPEN")
      {
        var style = new ol.style.Style(
          {
            zIndex: 2,
            image: new ol.style.Icon({
              scale: 0.17,
              src: 'assets/img/station_vert.png'
            }),
            text: new ol.style.Text({
              text: props.available_bikes.toString(),
              scale: 1.3,
              fill: new ol.style.Fill({
                color: '#ffffff'
              })
            })
          }
        );
      }
      else if (props.available_bike_stands < 5 && props.available_bike_stands > 0 && props.status == "OPEN"){
        var style = new ol.style.Style(
          {
            zIndex: 2,
            image: new ol.style.Icon({
              scale: 0.17,
              src: 'assets/img/station_orange.png'
            }),
            text: new ol.style.Text({
              text: props.available_bikes.toString(),
              scale: 1.3,
              fill: new ol.style.Fill({
                color: '#ffffff'
              })
            })
          }
        );
      }
      else if (props.status == "CLOSED"){
        var style = new ol.style.Style(
          {
            zIndex: 2,
            image: new ol.style.Icon({
              scale: 0.17,
              src: 'assets/img/station_noire.png'
            })
          }
        );
      }
      else
      {
        var style = new ol.style.Style(
          {
            zIndex: 2,
            image: new ol.style.Icon({
              scale: 0.17,
              src: 'assets/img/station_rouge.png'
            }),
            text: new ol.style.Text({
              text: props.available_bikes.toString(),
              scale: 1.3,
              fill: new ol.style.Fill({
                color: '#ffffff'
              })
            })
          }
        );
      }
      return [style];
    }
    if (this.filtre == 1)
    {
      this.filtre = 2;
      this.olMap.getLayers().item(2).getSource().getFeatures().forEach(function(feature){
        var prop = feature.getProperties();
        if (prop.status == "CLOSED")
        {
          feature.setStyle(emptyStyle);
        }
      });
    }
    else if (this.filtre == 2)
    {
      this.filtre = 3;
      this.olMap.getLayers().item(2).getSource().getFeatures().forEach(function(feature){
        var prop = feature.getProperties();
        feature.setStyle(station_style);
        if (prop.status != "CLOSED")
        {
          feature.setStyle(emptyStyle);
        }
      });
    }
    else if (this.filtre == 3)
    {
      this.filtre = 1;
      this.olMap.getLayers().item(2).getSource().getFeatures().forEach(function(feature){
          feature.setStyle(station_style);
      });
    }
  }

}
